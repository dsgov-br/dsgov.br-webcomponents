# Site Web Components - GovBR-DS

Esse site foi construído usando o [Docusaurus](https://docusaurus.io/).

## Como executar {#como-executar}

As dependências desse projeto são instaladas junto com as do resto do monorepo, mas caso deseje rodar esse projeto separado:

```bash
pnpm install
```

### Testar o site no localhost {#testar-o-site-no-localhost}

```bash
nx start site
```

Essa tarefa vai executar o build do projeto `@govbr-ds/webcomponents` e monitorar por mudanças, além de executar o site.

### Testar build de produção no localhost {#testar-build-de-produção-no-localhost}

Usado para testar o build do site no ambiente local.

```bash
nx serve site
```

## Scripts Disponíveis 📜 {#scripts-disponíveis-}

No arquivo `package.json` e `project.json`, você encontrará uma variedade de scripts úteis. Aqui está uma lista com a descrição de cada um:

```bash
# Executa o site no localhost para desenvolvimento.
nx start site
```

```bash
# Compila o site para produção.
nx build site
```

```bash
# Testa o build de produção no ambiente local.
nx serve site
```

```bash
# Cria uma nova versão da documentação do site.
nx docs:version site <versão>
```

```bash
# Permite customizar componentes do tema Docusaurus.
nx swizzle site
```

## MDX3 {#mdx3}

O formato de documentação aceito pelo Docusaurus v3 é o MDX3, dessa forma observe que algumas configurações do markdown padrão não vão funcionar nesse projeto.

## Versionar documentações {#versionar-documentações}

Ao versionar uma versão serão criadas pastas e arquivos separados para ela. A partir desse momento a versão será incluída na lista de seleção de versões e terá uma pasta específica para ela para o caso de ser necessário fazer alguma correção.

Para versionar:

```node
nx docs:version site <versão>
```

Para mais informações acesse <https://docusaurus.io/docs/versioning>.

### Arquivar versão de documentação {#arquivar-versão-de-documentação}

As versões arquivadas não aparecem na lista de seleção de versão, só na página com a listagem de todas as versões. Para marcar como arquivada siga os passos:

1. Mova a versão desejada do arquivo `versions.json` para o arquivo `versionsArchived.json`
   O formato é:

   ```json
   {
     "<numero da versão>": "<link-relativo-para-doc>"
   }
   ```

## Algolia {#algolia}

Para indexar nosso site usamos o Algolia Crawler. Ele faz a indexação todas as segundas às 9h da manhã, mas caso seja necessário fazer a indexação fora desse horário podem tentar acionar pelo endereço <https://crawler.algolia.com> (logando com a conta GovBR-DS) ou pode ser preciso entrar em contato com a equipe do crawler pelo discord.

## Arquitetura do projeto docusaurus {#arquitetura-do-projeto-docusaurus}

### Estrutura do projeto {#estrutura-do-projeto}

```markdown
site/
├── blog/
├── docs/
├── src/
│ ├── components/
│ └── pages/
├── static/
├── versioned_docs/
├── docusaurus.config.js
├── nodemon.json
├── sidebars.js
├── package.json
├── versions.json
├── versionsArchived.json
└── README.md
```

#### Descrição dos diretórios {#descrição-dos-diretórios}

- **blog/**: Contém os arquivos de postagem do blog em formato Markdown. Cada arquivo representa uma postagem e deve seguir o formato `YYYY-MM-DD-title.md`.

- **docs/**: Contém os arquivos de documentação em formato Markdown. Esses arquivos são organizados em pastas e subpastas conforme necessário.

- **src/**: Contém os arquivos de origem do site, incluindo componentes React e páginas personalizadas.

  - **components/**: Contém componentes React reutilizáveis que podem ser usados em várias páginas ou partes do site.

  - **css/**: Contém o CSS customizado para o site.

  - **pages/**: Contém arquivos React que representam páginas individuais. Essas páginas podem incluir conteúdo personalizado e usar componentes definidos em `src/components`.

  - **theme/**: Contém os componentes do tema padrão que estão sendo customizados.

- **static/**: Contém arquivos estáticos (imagens, fontes, etc.) que são copiados para a pasta de build sem serem processados.

- **versioned_docs/**: Só aparece quando uma versão é criada. Contém as documentações versionadas, organizadas por pastas com o nome das versões.

#### Arquivos de configuração {#arquivos-de-configuração}

- **docusaurus.config.js**: O principal arquivo de configuração para um projeto Docusaurus. Aqui, você pode definir várias opções, como título do site, configurações do tema, plugins, entre outros.

- **sidebars.js**: Define a estrutura das barras laterais de navegação para a documentação. Você pode especificar a ordem dos documentos e como eles são agrupados.

- **nodemon.json**: Define as regras de páginas que devem ser observadas por mudanças e as tarefas que serão executadas.

- **package.json**: Contém as dependências do projeto e scripts úteis para construir e desenvolver o site.

- **versions.json**: Lista com os números de versões criadas.

- **versionsArchived.json**: Lista das versões anteriormente criadas e agora arquivadas.

### Fluxo de trabalho {#fluxo-de-trabalho}

1. **Escrever Conteúdo**: Crie os documentos no diretório `docs/` e posts de blog no diretório `blog/`.
1. **Pré-visualizar**: Use o script `dev` para visualizar suas alterações localmente.
1. **Compilar**: Use o script `build` para testar se o processo de compilação está funcionando corretamente.

### Pasta `docs` {#pasta-docs}

Os documentos são usados para compartilhar informações sobre a biblioteca e assuntos relacionados. Como instalar a biblioteca, quais componentes disponibilizamos e como usá-los são exemplos de documentos.

Todos os documentos **devem** ter o frontmatter para que seja corretamente renderizado.

Os documentos relacionados a documentação de componentes (`docs/components/`) tem um modelo básico que deve ser customizado, mas seguindo o padrão já usado nos outros:

```md
---
title: Componente
description: Componente ABC
---

<!-- Componentes React -->

import CustomSandpack from '@site/src/components/CustomSandpack';

<!-- Seções -->

import Dependencies from '../stencil-generated-docs/abc/dependencies.md';
import Migrate from '../stencil-generated-docs/abc/sections/migrate.md';
import Overview from '../stencil-generated-docs/abc/overview.md';
import Props from '../stencil-generated-docs/abc/props.md';
import Slots from '../stencil-generated-docs/abc/slots.md';

<!-- Exemplos -->

import example1 from '!!raw-loader!../stencil-generated-docs/abc/examples/example1.html'

<!-- Javascript -->

import javascript from '!!raw-loader!../stencil-generated-docs/abc/examples/index.js'

<Overview/>
<Props/>

## Exemplos

<CustomSandpack htmlCode={example1} jsCode={javascript} title="Lorem"/>

<Slots/>
<Dependencies/>
<Migrate/>
```

#### Pasta `stencil-generated-docs` {#pasta-stencil-generated-docs}

Fica dentro da pasta `docs` e é usada para guardar as documentações, exemplos e JS dos componentes gerados pelo stencil. Essa pasta é gerada automaticamente e **toda** ela é deletada ao gerar um novo build do `stencil`. Sendo assim, nunca coloque arquivos dentro dela para consulta futura pois todos serão deletados.

### Componentes {#componentes}

Componentes react dentro da pasta `site/src/components/`.

/*cspell:disable-next-line*/

#### HomepageFeatures {#homepagefeatures}

Página inicial do site.

/*cspell:disable-next-line*/

#### CustomSandpack {#customsandpack}

/*cspell:disable-next-line*/
Cria um preview de um componente.

### Recursos Adicionais {#recursos-adicionais}

Para mais informações, consulte a [documentação oficial do Docusaurus](https://docusaurus.io/docs).
