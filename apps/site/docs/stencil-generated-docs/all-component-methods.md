| Assinatura                                          | Componente  | Descrição                                                                                                         |
| --------------------------------------------------- | ----------- | ----------------------------------------------------------------------------------------------------------------- |
| `hide() => Promise<{ isOpen: boolean; }>`           | br-dropdown | Esconde o dropdown. Define a propriedade `isOpen` como falsa e retorna o novo estado. Este método é marcado com   |
| `open() => Promise<{ isOpen: boolean; }>`           | br-dropdown | Abre o dropdown. Define a propriedade `isOpen` como verdadeira e retorna o novo estado. Este método é marcado com |
| `setIndeterminate(value: boolean) => Promise<void>` | br-checkbox | Define o estado indeterminado do checkbox.                                                                        |
| `setValue(newValue: string) => Promise<void>`       | br-textarea | Define um novo valor para o textarea.                                                                             |
| `toggleChecked() => Promise<void>`                  | br-checkbox | Inverte o valor da prop `checked`                                                                                 |
| `toggleChecked() => Promise<void>`                  | br-radio    | Inverte o valor da prop `checked`                                                                                 |
| `toggleChecked() => Promise<void>`                  | br-switch   | Inverte o valor da prop `checked`                                                                                 |
| `toggleOpen() => Promise<void>`                     | br-select   | Inverte o valor da prop `isOpen`                                                                                  |
