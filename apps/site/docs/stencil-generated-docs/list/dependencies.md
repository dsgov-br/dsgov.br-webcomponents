## Dependências

### Usado por

- [br-select](../select)

### Gráfico

```mermaid
graph TD;
  br-select --Depende---> br-list
  click br-list "src/components/list" "Link para a documentação do componente list"
  class br-list depComponent
  class br-list mainComponent
```
