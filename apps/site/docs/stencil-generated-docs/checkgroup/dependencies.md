## Dependências

### Depende de

- [br-checkbox](../checkbox)

### Gráfico

```mermaid
graph TD;
  br-checkgroup --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  class br-checkgroup mainComponent
```
