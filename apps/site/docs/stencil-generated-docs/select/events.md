## Eventos

| Evento              | Descrição                                                            | Tipo               |
| ------------------- | -------------------------------------------------------------------- | ------------------ |
| `brDidSelectChange` | Evento emitido sempre que houver atualização nos itens selecionados. | `CustomEvent<any>` |
