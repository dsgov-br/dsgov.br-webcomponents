## Dependências

### Depende de

- [br-button](../button)
- [br-icon](../icon)

### Gráfico

```mermaid
graph TD;
  br-upload --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-upload --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  class br-upload mainComponent
```
