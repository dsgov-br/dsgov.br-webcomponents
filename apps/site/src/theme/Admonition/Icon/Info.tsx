export default function AdmonitionIconInfo(): JSX.Element {
  return (
    <div className="icon">
      <i className="fas fa-info-circle fa-lg" aria-hidden="true"></i>
    </div>
  )
}
