export default function AdmonitionIconCaution(): JSX.Element {
  return (
    <div className="icon">
      <i className="fas fa-exclamation-triangle fa-lg" aria-hidden="true"></i>
    </div>
  )
}
