import Translate from '@docusaurus/Translate'
import IconNote from '@theme/Admonition/Icon/Note'
import AdmonitionLayout from '@theme/Admonition/Layout'
import type { Props } from '@theme/Admonition/Type/Note'
import clsx from 'clsx'

const infimaClassName = 'alert alert--secondary'

const defaultProps = {
  icon: <IconNote />,
  title: (
    <Translate id="theme.admonition.note" description="The default label used for the Note admonition (:::note)">
      note
    </Translate>
  ),
}

export default function AdmonitionTypeNote(props: Props): JSX.Element {
  return (
    <AdmonitionLayout {...defaultProps} {...props} className={clsx(infimaClassName, props.className)}>
      {props.children}
    </AdmonitionLayout>
  )
}
