import Translate from '@docusaurus/Translate'
import IconTip from '@theme/Admonition/Icon/Tip'
import AdmonitionLayout from '@theme/Admonition/Layout'
import type { Props } from '@theme/Admonition/Type/Tip'
import clsx from 'clsx'

const infimaClassName = 'alert alert--success'

const defaultProps = {
  icon: <IconTip />,
  title: (
    <Translate id="theme.admonition.tip" description="The default label used for the Tip admonition (:::tip)">
      tip
    </Translate>
  ),
}

export default function AdmonitionTypeTip(props: Props): JSX.Element {
  return (
    <AdmonitionLayout {...defaultProps} {...props} className={clsx(infimaClassName, 'success')}>
      {props.children}
    </AdmonitionLayout>
  )
}
