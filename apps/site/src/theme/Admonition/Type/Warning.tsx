import Translate from '@docusaurus/Translate'
import IconWarning from '@theme/Admonition/Icon/Warning'
import AdmonitionLayout from '@theme/Admonition/Layout'
import type { Props } from '@theme/Admonition/Type/Warning'
import clsx from 'clsx'

const infimaClassName = 'alert alert--warning'

const defaultProps = {
  icon: <IconWarning />,
  title: (
    <Translate
      id="theme.admonition.warning"
      description="The default label used for the Warning admonition (:::warning)"
    >
      warning
    </Translate>
  ),
}

export default function AdmonitionTypeWarning(props: Props): JSX.Element {
  return (
    <AdmonitionLayout {...defaultProps} {...props} className={clsx(infimaClassName, 'warning')}>
      {props.children}
    </AdmonitionLayout>
  )
}
