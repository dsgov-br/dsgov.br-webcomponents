import useDocusaurusContext from '@docusaurus/useDocusaurusContext'

export default function NotFoundContent() {
  const {
    siteConfig: { baseUrl },
  } = useDocusaurusContext()

  return (
    <main className={'d-flex flex-fill flex-column justify-content-center align-items-center'}>
      <div className="d-flex justify-content-center align-items-center">
        <img src={`${baseUrl}/img/page404.png`} alt="Imagem de página não encontrada" />
        <div>
          <p className="text-up-06 text-semi-bold my-3">Algo deu errado...</p>
          <p className="text-up-03 text-medium my-3">
            Não encontramos a página que procurava, mas disponibilizamos alguns links que podem ser úteis.
          </p>
        </div>
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <button className="br-button crumb" type="button" onClick={() => window.history.back()}>
          <i className="fas fa-chevron-left" aria-hidden="true"></i>
          <span>Ir para Página Anterior</span>
        </button>
        <button className="br-button crumb" type="button" onClick={() => (window.location.href = baseUrl)}>
          <i className="fas fa-home" aria-hidden="true"></i>
          <span>Ir para Página Principal</span>
        </button>
        <button
          className="br-button crumb"
          type="button"
          onClick={() =>
            (window.location.href =
              'https://docs.google.com/forms/d/e/1FAIpQLSdQe4a_WMAVkm1pIv_eRMYEmqE8EP1oyqylDjxO_dHwqIxrSQ/viewform?usp=sf_link')
          }
        >
          <i className="fas fa-comment-dots" aria-hidden="true"></i>
          <span>Envie um Feedback</span>
        </button>
      </div>
    </main>
  )
}
