module.exports = function (plop) {
  plop.setGenerator('component', {
    description: 'Criar um novo Web Component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Qual é o nome do componente? (sem prefixo "br" e em kebab-case)',
      },
      {
        type: 'confirm',
        name: 'addFile',
        message: 'Adicionar arquivos de exemplo?',
        default: false,
      },
      {
        type: 'input',
        name: 'examples',
        message: 'Quais são os nomes dos exemplos? (separados por vírgula, sem extensão `.html` e em kebab-case)',
        when: (data) => data.addFile, // Só pergunta pelos exemplos se addFile for verdadeiro
      },
    ],
    actions: function (data) {
      // Usa o optional chaining para acessar 'examples' com segurança e garantir que seja um array
      const exampleActions =
        data.examples?.split(',').map((example) => {
          const formattedExample = example.trim()
          return {
            type: 'add',
            skipIfExists: true,
            path: `packages/webcomponents/src/pages/components/{{name}}/${formattedExample}.html`,
            templateFile: 'plop-templates/Page.html.hbs',
            data: {
              example: formattedExample,
            },
          }
        }) || [] // Se data.examples for indefinido ou nulo, utiliza um array vazio

      // Adiciona a ação para o index.js condicionalmente, se addFile for verdadeiro
      const indexJsAction = data.addFile
        ? {
            type: 'add',
            skipIfExists: true,
            path: 'packages/webcomponents/src/pages/components/{{name}}/index.js',
            templateFile: 'plop-templates/Page.js.hbs',
          }
        : null // Se addFile for falso, nenhuma ação para index.js

      // Adiciona a ação para o menu.js condicionalmente, se addFile for verdadeiro e houver exemplos
      const menuJsAction =
        data.addFile && data.examples?.split(',').length > 0
          ? {
              path: 'packages/webcomponents/src/pages/scripts/menu.js',
              pattern: /(\/\/ NOVOS COMPONENTES - NÃO DELETAR)/g,
              templateFile: 'plop-templates/menuTemplate.hbs',
              type: 'modify',
            }
          : null // Se addFile for falso ou não houver exemplos, nenhuma ação para menu.js

      // Filtra valores nulos (caso indexJsAction ou menuJsAction não sejam necessários)
      const actions = [
        {
          type: 'add',
          skipIfExists: true,
          path: 'packages/webcomponents/src/components/{{name}}/{{name}}.tsx',
          templateFile: 'plop-templates/Component.tsx.hbs',
        },
        {
          type: 'add',
          skipIfExists: true,
          path: 'packages/webcomponents/src/components/{{name}}/{{name}}.scss',
          templateFile: 'plop-templates/Component.scss.hbs',
        },
        {
          type: 'add',
          skipIfExists: true,
          path: 'packages/webcomponents/src/components/{{name}}/_tests/{{name}}.spec.tsx',
          templateFile: 'plop-templates/Component.spec.tsx.hbs',
        },
        {
          type: 'add',
          skipIfExists: true,
          path: 'packages/webcomponents/src/components/{{name}}/_tests/{{name}}.e2e.ts',
          templateFile: 'plop-templates/Component.e2e.ts.hbs',
        },
        {
          type: 'add',
          skipIfExists: true,
          path: 'packages/webcomponents/src/components/{{name}}/sections/migrate.md',
          templateFile: 'plop-templates/migrate.md.hbs',
        },
        {
          type: 'add',
          skipIfExists: true,
          path: 'apps/site/docs/components/{{name}}.mdx',
          templateFile: 'plop-templates/docs.md.hbs',
        },
        ...exampleActions, // Adiciona as ações para os exemplos dinamicamente
      ]

      // Adiciona a ação do index.js somente se estiver definida (ou seja, se addFile for verdadeiro)
      if (indexJsAction) {
        actions.push(indexJsAction)
      }

      // Adiciona a ação do menu.js somente se estiver definida (ou seja, se addFile for verdadeiro e houver exemplos)
      if (menuJsAction) {
        actions.push(menuJsAction)
      }

      return actions
    },
  })
}

const handlebars = require('handlebars')

// Helper to split a comma-separated string into an array of PascalCase file names
handlebars.registerHelper('splitAndPascal', function (input) {
  if (!input || typeof input !== 'string') {
    return [] // Return an empty array if input is undefined, null, or not a string
  }

  // Process the input: split by commas, trim spaces, and convert each item to PascalCase
  return input.split(',').map((example) => {
    return example
      .trim()
      .replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match) => match.toUpperCase()) // Capitalize the first letter of each word
      .replace(/[-\s]+/g, '') // Remove any hyphens or spaces between words
  })
})

handlebars.registerHelper('splitAndKebab', function (input) {
  if (!input || typeof input !== 'string') {
    return [] // Return an empty array if input is undefined, null, or not a string
  }

  // Process the input: split by commas, trim spaces, convert each item to kebab-case, and wrap in quotes
  return input.split(',').map((example) => {
    const kebabCase = example
      .trim()
      .toLowerCase()
      .replace(/[^a-z0-9]+/g, '-') // Replace non-alphanumeric characters with hyphens
      .replace(/^-+|-+$/g, '') // Remove leading and trailing hyphens
    return `"${kebabCase}"` // Wrap in quotes
  })
})

// Helper para formatar uma string em PascalCase com remoção de hifens
handlebars.registerHelper('toPascalCase', function (input) {
  return (
    input
      // Substitui os hifens por espaços para tratá-los como separadores de palavras
      .replace(/-./g, (match) => match.charAt(1).toUpperCase()) // Converte a letra após o hífen para maiúscula
      // Converte a primeira letra da primeira palavra para maiúscula
      .replace(/^./, (match) => match.toUpperCase())
  )
})

handlebars.registerHelper('toKebabCase', function (input) {
  return (
    input
      // Convert uppercase letters to lowercase and add a hyphen before them (except the first letter)
      .replace(/[A-Z]/g, (match) => `-${match.toLowerCase()}`)
      // Remove leading hyphen if it exists
      .replace(/^-/, '')
  )
})

handlebars.registerHelper('jsxCurlyBrace', function (value) {
  return new handlebars.SafeString(`{${value}}`)
})
