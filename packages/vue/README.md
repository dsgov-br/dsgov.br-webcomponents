# Wrapper Vue para @govbr-ds/webcomponents

Este wrapper Vue encapsula os [Web Components GovBR-DS](https://gov.br/ds/webcomponents), permitindo que sejam utilizados como componentes nativos no Vue.

## Por que usar este wrapper? 🤔

Usar o wrapper Vue para os Web Components GovBR-DS oferece várias vantagens:

- **Verificação de Tipos**: Permite a verificação de tipos com seus componentes, garantindo maior segurança e previsibilidade no desenvolvimento.

- **Integração com Vue Router**: Facilita a integração com o Vue Router, permitindo o uso de componentes como links de roteamento.

- **Suporte ao v-model**: Componentes de controle de formulário podem ser usados com `v-model`, proporcionando uma experiência de desenvolvimento mais intuitiva e eficiente.

Para mais detalhes, consulte a [documentação oficial do Stencil](https://stenciljs.com/docs/vue).

## Instalação ⚙️

Para instalar os pacotes necessários, execute:

```bash
npm install --save-dev @govbr-ds/{core,webcomponents,webcomponents-vue}
```

## Configuração 🛠️

### Importação dos tokens de estilo do `@govbr-ds/core`

Adicione os estilos do `govbr-ds/core` ao arquivo principal de estilos do seu projeto:

```css
@import '~@govbr-ds/core/dist/core-tokens.min.css';
```

### Configuração do Vue

Configure o Vue para reconhecer as tags dos Web Components como custom elements:

```js
// vite.config.js
...
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          // Permite que Vue trate tags de componentes como custom elements
          isCustomElement: (tag) => tag.includes("br-"),
        },
      },
    }),
  ],
  ...
});
```

## Uso 📚

Importe e utilize os componentes encapsulados como qualquer componente Vue:

```typescript
import { BrButton } from '@govbr-ds/webcomponents-vue'
```

Exemplo de uso com `v-model`:

```jsx
<script setup lang="ts">
const name = ref('Lorem ipsum');
</script>

<template>
  <h1>Olá {{ name }}</h1>
  <br-input
    name="name"
    placeholder="Por favor, digite seu nome..."
    v-model="name"
  >
  </br-input>
</template>
```

## Estrutura de pastas e arquivos 📁

```markdown
- src
  - stencil-generated
  - index.ts
```

A pasta `src` contém os arquivos padrão da biblioteca Vue.

Ao gerar um build da biblioteca de Web Components, os arquivos da pasta `src/stencil-generated` é recriada. **Todas as alterações nesses arquivos serão perdidas ao gerar um novo build dos Web Components.**

Os arquivos `index.ts` são os entry points da biblioteca.

## Build 📦

Antes de compilar a biblioteca Vue, é necessário gerar o build dos webcomponents:

```bash
nx build webcomponents
```

Em seguida, para gerar o build da biblioteca Vue, execute:

```bash
nx build vue
```

## Documentações Complementares 📚

Consulte a seção sobre Web Componente na nossa [Wiki](https://gov.br/ds/wiki/desenvolvimento/web-components) para obter mais informações sobre esse projeto.

Para saber mais detalhes sobre a especificação Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Contribuindo 🤝

Antes de abrir um Merge Request, por favor, leve em consideração as seguintes informações:

- Este é um projeto open-source e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, use um título claro e explicativo para o MR, e siga os padrões estabelecidos em nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir? Consulte o nosso guia [como contribuir](https://gov.br/ds/wiki/comunidade/contribuindo-com-o-ds/ 'Como contribuir?').

## Reportar Bugs/Problemas ou Sugestões 🐛

Para reportar problemas ou sugerir melhorias, abra uma [issue](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/issues/new). Utilize o modelo adequado e forneça o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Commits 📝

Este projeto segue um padrão para branches e commits. Consulte a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender mais sobre esses padrões.

## Precisa de ajuda? 🆘

Por favor, **não** crie issues para perguntas gerais.

Use os canais abaixo para tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)
- Web Components [https://gov.br/ds/webcomponents](https://gov.br/ds/webcomponents)
- Canal no Discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Créditos 🎉

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') foram desenvolvidos pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') em colaboração com a comunidade.

# Vue.js Wrapper for BEEQ

A lightweight utility that wraps BEEQ custom elements ("Web Components") so they can be seamlessly integrated into Vue applications.

## Why is this necessary?

BEEQ can generate Vue component wrappers for your Web Components. This allows BEEQ components to be used within a Vue 3 application. The benefits of using BEEQ's component wrappers over the standard Web Components include:

- Type checking with your components.
- Integration with the router link and Vue router.
- Optionally, form control Web Components can be used with `v-model`.

(*Adapted from the [Stencil official documentation](https://stenciljs.com/docs/vue)*)

## Package installation

> [!TIP]
> Please always refer to the [official BEEQ documentation](https://www.beeq.design/3d466e231/p/359a26-for-developers/b/53475d) for more information about the installation.

- install the package

```
npm install @beeq/{core,vue}
```

> [!NOTE]
> Make sure that you have installed the `@beeq/core` package.

### Add BEEQ styles and assets

Import BEEQ styles into your application's main style file:

```css
@import "@beeq/core/dist/beeq/beeq.css";
```

> [!TIP]
> BEEQ uses SVG icons and these assets are shipped in a separate folder. You can use the `setBasePath` method to set the path to the icons. Make sure that your project bundle the icons in a way that they are accessible from the browser.

You can move the icons from the node_modules folder to your assets folder and set the path like this:

```js
// vite.config.js
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { viteStaticCopy } from "vite-plugin-static-copy";

export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          // treat all BEEQ tags as custom elements
          isCustomElement: (tag) => tag.includes("bq-"),
        },
      },
    }),
    vueJsx(),
    viteStaticCopy({
      targets: [
        {
          src: "./node_modules/@beeq/core/dist/beeq/svg/*",
          dest: "assets/svg",
        },
        // other assets
      ],
    }),
  ],
  // other configurations
});
```

```js
// main.ts
import { setBasePath } from '@beeq/core';

setBasePath('icons/svg');
```

But you can also use a different icons library or a CDN:

```js
import { setBasePath } from '@beeq/core';

// Using heroicons library
setBasePath('https://cdn.jsdelivr.net/npm/heroicons@2.1.5/24/outline');
```

> [!CAUTION]
> When using a different icons library, make sure you use the correct icon names provided by the library or the CDN.

## Usage

```jsx
<script setup lang="ts">
const name = ref('John Doe');
</script>

<template>
  <h1>Hello {{ name }}</h1>
  <bq-input
    name="name"
    placeholder="Please type your name..."
    v-model="name"
    @bqClear="name = ''"
  >
    <label slot="label">Your name</label>
    <bq-icon name="user" slot="prefix"></bq-icon>
  </bq-input>
</template>
```
