/**
 * @fileoverview ponto de entrada para sua biblioteca de componentes
 *
 * Este é o ponto de entrada para sua biblioteca de componentes. Use este arquivo para exportar utilitários,
 * constantes ou estruturas de dados que acompanham seus componentes.
 *
 * NÃO use este arquivo para exportar seus componentes. Em vez disso, use as abordagens recomendadas
 * para consumir os componentes deste pacote, conforme descrito no `README.md`.
 */

// export { format } from './utils/utils'
export type * from './components.d.ts'
