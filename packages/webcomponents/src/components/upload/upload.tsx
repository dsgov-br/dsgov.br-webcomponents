import { AttachInternals, Component, Element, h, Host, Prop, State } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

enum UploadState {
  INFO = 'info',
  WARNING = 'warning',
  DANGER = 'danger',
  SUCCESS = 'success',
}

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/upload?tab=designer).
 *
 * @slot default - Descrição do slot
 */
@Component({
  tag: 'br-upload',
  styleUrl: 'upload.scss',
  shadow: true,
  formAssociated: true,
})
export class Upload {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el!: HTMLBrUploadElement

  /**
   * Tipos de arquivo permitidos (ex: 'image/*').
   * Esta propriedade define quais tipos de arquivos podem ser selecionados para upload.
   */
  @Prop() accept: string = ''

  /**
   * Texto descritivo do campo de upload.
   * Este é o texto que é mostrado no label do textarea.
   */
  @Prop() label: string = 'Envio de arquivo'

  /**
   * Evento emitido quando um arquivo é selecionado.
   * Este estado armazena os arquivos que foram selecionados pelo usuário.
   */
  @State() filesSelected: FileList | null = null

  /**
   * Referência ao elemento de arquivo selecionado.
   * Este estado armazena o arquivo que foi selecionado para upload.
   */
  @State() file: File | null = null

  /**
   * Indica se o componente está desativado.
   * Quando definido como `true`, o campo de upload se torna não interativo,
   * impedindo que o usuário selecione ou envie arquivos.
   * Isso é útil para controlar a interação do usuário em situações específicas,
   * como durante o carregamento de dados ou quando o formulário está em um estado inválido.
   */
  @Prop({ reflect: true }) disabled: boolean = false

  /**
   * Define o estado visual da mensagem.
   * Os possíveis valores são:
   * - 'info': Mensagem informativa.
   * - 'warning': Mensagem de aviso.
   * - 'danger': Mensagem de erro ou alerta.
   * - 'success': Mensagem de sucesso.
   * O valor padrão é 'info'.
   */
  @Prop({ reflect: true }) state: string = UploadState.INFO

  /**
   * Indica se o componente permite a seleção de múltiplos arquivos.
   * Quando definido como `true`, o usuário pode selecionar mais de um arquivo para upload.
   */
  @Prop() multiple: boolean = false

  @AttachInternals() elementInternals: ElementInternals

  private getCssClassMap(): CssClassMap {
    return {
      'br-upload': true,
      [this.state]: true,
    }
  }

  private handleRemoveFileSelection(file: File) {
    const newArrayFilter = Array.from(this.filesSelected).filter((f) => f !== file)
    this.filesSelected = newArrayFilter as unknown as FileList
    const fileInput = this.el.shadowRoot.getElementById('single-file') as HTMLInputElement
    fileInput.value = ''
  }

  private renderFilesUploaded() {
    const fileSizes = this._calculateFileSize(this.filesSelected)

    return fileSizes.map(({ file, size }) => {
      return (
        <div class="br-item d-flex" key={file.name}>
          <div class="content text-primary-default mr-auto">{file.name}</div>
          <div class="support mr-n2">
            <span class="">{size}</span>
            <br-button
              shape="circle"
              type="button"
              aria-label="Remover arquivo"
              onClick={() => this.handleRemoveFileSelection(file)}
            >
              <br-icon icon-name="fa-solid:trash" aria-hidden="true"></br-icon>
            </br-button>
          </div>
        </div>
      )
    })
  }

  private onInputChange(files: FileList) {
    if (files.length === 1) {
      this.filesSelected = files
    } else if (files.length > 0) {
      this.filesSelected = files
    } else {
      console.error(files.length === 0 ? 'NENHUM ARQUIVO ENVIADO' : 'VOCÊ SÓ PODE ENVIAR UM ARQUIVO POR VEZ')

      return false
    }
  }

  private _calculateFileSize(files: FileList) {
    return Array.from(files).map((file) => {
      let sOutput = ''
      const aMultiples = ['KB', 'MB', 'GB', 'TB']
      let nMultiple = 0
      let nApprox = file.size / 1024

      if (nApprox < 1) {
        sOutput = `${file.size} bytes`
      } else {
        while (nApprox > 1 && nMultiple < aMultiples.length) {
          sOutput = `${nApprox.toFixed(2)} ${aMultiples[nMultiple]}`
          nApprox /= 1024
          nMultiple++
        }
      }

      return { file: file, size: sOutput || 0 }
    })
  }

  private handleDrop(event: DragEvent) {
    event.preventDefault()
    const files = event.dataTransfer?.files
    if (files && files.length > 0) {
      this.onInputChange(files)
    }
  }

  private handleDragOver(event: DragEvent) {
    event.preventDefault()
  }

  private handleFileSelection() {
    const fileInput = this.el.shadowRoot.getElementById('single-file') as HTMLInputElement
    fileInput.click()
  }

  private readonly handleInputChange = (event: Event) => {
    const target = event.target as HTMLInputElement
    this.onInputChange(target.files)
  }

  render() {
    return (
      <Host>
        <div class={this.getCssClassMap()}>
          <label class="upload-label" form="single-file">
            <span>{this.label}</span>
          </label>
          <button
            class="upload-button"
            type="button"
            tabIndex={this.disabled ? -1 : 0}
            disabled={this.disabled}
            aria-label="Selecionar arquivo para upload"
            onClick={() => this.handleFileSelection()}
            onDrop={(event) => this.handleDrop(event)}
            onDragOver={(event) => this.handleDragOver(event)}
          >
            <br-icon icon-name="fa-solid:upload" height="16" aria-hidden="true"></br-icon>
            <span>Selecione o arquivo</span>
          </button>

          <input
            class="upload-input"
            id="single-file"
            type="file"
            aria-label="enviar arquivo"
            onChange={this.handleInputChange}
            multiple={this.multiple}
            disabled={this.disabled}
            accept={this.accept}
          />
          <div id="list-files" class="upload-list">
            {this.filesSelected ? this.renderFilesUploaded() : null}
          </div>
        </div>
      </Host>
    )
  }
}
