import { ComponentModelConfig } from '@stencil/vue-output-target'

export const vueComponentModels: ComponentModelConfig[] = [
  {
    elements: ['br-input', 'br-textarea'],
    event: 'valueChange',
    targetAttr: 'value',
  },
  // {
  //   elements: ['br-select'],
  //   event: 'brDidSelectChange',
  //   targetAttr: 'inputValue',
  // },
  {
    elements: ['br-checkbox', 'br-radio', 'br-switch'],
    event: 'checkedChange',
    targetAttr: 'checked',
  },
]
