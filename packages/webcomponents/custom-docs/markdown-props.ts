import { JsonDocsProp } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

export const propsToMarkdown = (props: JsonDocsProp[]) => {
  const content: string[] = []

  // Verifica se há propriedades a serem processadas
  if (props.length === 0) return content

  content.push(`## Propriedades`) // Adiciona o título da seção
  content.push(``)

  const table = new MarkdownTable() // Cria uma nova tabela Markdown

  // Adiciona o cabeçalho da tabela
  table.addHeader(['Propriedade', 'Atributo', 'Descrição', 'Tipo', 'Valor padrão'])

  // Itera sobre os metadados das propriedades
  props.forEach((prop) => {
    // Adiciona uma linha na tabela para cada propriedade
    table.addRow([
      getPropertyField(prop),
      getAttributeField(prop),
      getDocsField(prop),
      getTypeField(prop),
      getDefaultValueField(prop),
    ])
  })

  // Adiciona a tabela convertida ao conteúdo
  content.push(...table.toMarkdown())
  content.push(``)

  return content // Retorna o conteúdo gerado
}

// Formata o campo da propriedade
const getPropertyField = (prop: JsonDocsProp) => {
  return `\`${prop.name}\`${prop.required ? ' *(obrigatório)*' : ''}`
}

// Formata o campo do atributo
const getAttributeField = (prop: JsonDocsProp) => {
  return prop.attr && prop.attr.length > 0 ? `\`${prop.attr}\`` : '---'
}

// Formata o campo da documentação
const getDocsField = (prop: JsonDocsProp) => {
  return `${
    prop.deprecation !== undefined
      ? `<span class="deprecated">**[Descontinuado/Obsoleto]**</span> ${prop.deprecation}<br/><br/>`
      : ''
  }${prop.docs}`
}

// Formata o campo do tipo
const getTypeField = (prop: JsonDocsProp) => {
  return prop.type.includes('`') ? `\`\` ${prop.type} \`\`` : `\`${prop.type}\``
}

// Formata o campo do valor padrão
const getDefaultValueField = (prop: JsonDocsProp) => {
  if (prop.default === undefined || prop.default.trim() === '') return '---'

  // Se existir, formata o valor
  return prop.default.includes('`') ? `\`\`${prop.default}\`\`` : `\`${prop.default}\``
}
