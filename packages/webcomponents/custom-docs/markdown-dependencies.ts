import { JsonDocsComponent } from '@stencil/core/internal'

import { normalizePath, relative } from './path'

// Função para gerar a documentação em Markdown das dependências de um componente
export const depsToMarkdown = (cmp: JsonDocsComponent, cmps: JsonDocsComponent[]) => {
  const content: string[] = []

  // Obtém as entradas do gráfico de dependências do componente
  const deps = Object.entries(cmp.dependencyGraph)

  // Se não houver dependências, retorna um array vazio
  if (deps.length === 0) return content

  // Adiciona o título da seção de dependências
  content.push(`## Dependências`)
  content.push(``)

  // Se o componente tiver dependentes, lista-os
  if (cmp.dependents.length > 0) {
    const usedBy = cmp.dependents.map((tag) => ' - ' + getCmpLink(cmp, tag, cmps)) // Formata a lista de dependente
    content.push(`### Usado por`)
    content.push(``)
    content.push(...usedBy) // Adiciona a lista ao conteúdo
    content.push(``)
  }

  // Se o componente tiver dependências, liste-as
  if (cmp.dependencies.length > 0) {
    const dependsOn = cmp.dependencies.map((tag) => '- ' + getCmpLink(cmp, tag, cmps)) // Formata a lista de dependências

    content.push(`### Depende de`)
    content.push(``)
    content.push(...dependsOn) // Adiciona a lista ao conteúdo
    content.push(``)
  }

  // Adiciona o gráfico das dependências em formato Mermaid
  content.push(`### Gráfico`)
  content.push('```mermaid')
  content.push('graph TD;')
  deps.forEach(([key, dep]) => {
    dep.forEach((dependency) => {
      content.push(`  ${key} --Depende---> ${dependency}`) // Cria a ligação entre os nós
      content.push(
        `  click ${dependency} "src/components/${dependency.replace('br-', '')}" "Link para a documentação do componente ${dependency.replace('br-', '')}"`
      ) // Cria os links para redirecionar ao clicar nos nós
      content.push(`  class ${dependency} depComponent`) // Define a classe para estilizar as dependências
    })
  })

  content.push(`  class ${cmp.tag} mainComponent`) // Define a classe para estilizar o componente atual
  content.push('```') // Fecha a seção do gráfico
  content.push(``) // Linha em branco no final para formatar o Markdown

  return content
}

// Função auxiliar para obter o link de um componente
const getCmpLink = (from: JsonDocsComponent, to: string, cmps: JsonDocsComponent[]) => {
  const destCmp = cmps.find((c) => c.tag === to) // Encontra o componente destino
  if (destCmp !== undefined) {
    // Normaliza o caminho relativo entre os componentes
    const cmpRelPath = normalizePath(relative(from.dirPath || '', destCmp.dirPath || ''))
    return `[${to}](${cmpRelPath})` // Retorna o link formatado em Markdown
  }
  return to // Retorna o nome do componente se não encontrado
}
