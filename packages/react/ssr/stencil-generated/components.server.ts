/**
 * This file was automatically generated by the Stencil React Output Target.
 * Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
 * Do __not__ import components from this file as server side rendered components
 * may not hydrate due to missing Stencil runtime. Instead, import these components through the generated 'components.ts'
 * file that re-exports all components with the 'use client' directive.
 */

/* eslint-disable */

import { BrAvatar as BrAvatarElement, defineCustomElement as defineBrAvatar } from "@govbr-ds/webcomponents/dist/components/br-avatar.js";
import { BrButton as BrButtonElement, defineCustomElement as defineBrButton } from "@govbr-ds/webcomponents/dist/components/br-button.js";
import { BrCheckbox as BrCheckboxElement, defineCustomElement as defineBrCheckbox } from "@govbr-ds/webcomponents/dist/components/br-checkbox.js";
import { BrCheckgroup as BrCheckgroupElement, defineCustomElement as defineBrCheckgroup } from "@govbr-ds/webcomponents/dist/components/br-checkgroup.js";
import { BrDropdown as BrDropdownElement, defineCustomElement as defineBrDropdown } from "@govbr-ds/webcomponents/dist/components/br-dropdown.js";
import { BrIcon as BrIconElement, defineCustomElement as defineBrIcon } from "@govbr-ds/webcomponents/dist/components/br-icon.js";
import { BrInput as BrInputElement, defineCustomElement as defineBrInput } from "@govbr-ds/webcomponents/dist/components/br-input.js";
import { BrItem as BrItemElement, defineCustomElement as defineBrItem } from "@govbr-ds/webcomponents/dist/components/br-item.js";
import { BrList as BrListElement, defineCustomElement as defineBrList } from "@govbr-ds/webcomponents/dist/components/br-list.js";
import { BrLoading as BrLoadingElement, defineCustomElement as defineBrLoading } from "@govbr-ds/webcomponents/dist/components/br-loading.js";
import { BrMessage as BrMessageElement, defineCustomElement as defineBrMessage } from "@govbr-ds/webcomponents/dist/components/br-message.js";
import { BrRadio as BrRadioElement, defineCustomElement as defineBrRadio } from "@govbr-ds/webcomponents/dist/components/br-radio.js";
import { BrSelect as BrSelectElement, defineCustomElement as defineBrSelect } from "@govbr-ds/webcomponents/dist/components/br-select.js";
import { BrSwitch as BrSwitchElement, defineCustomElement as defineBrSwitch } from "@govbr-ds/webcomponents/dist/components/br-switch.js";
import { BrTag as BrTagElement, defineCustomElement as defineBrTag } from "@govbr-ds/webcomponents/dist/components/br-tag.js";
import { BrTextarea as BrTextareaElement, defineCustomElement as defineBrTextarea } from "@govbr-ds/webcomponents/dist/components/br-textarea.js";
import { BrUpload as BrUploadElement, defineCustomElement as defineBrUpload } from "@govbr-ds/webcomponents/dist/components/br-upload.js";
import type { EventName, StencilReactComponent } from '@stencil/react-output-target/runtime';
import { createComponent, createSSRComponent } from '@stencil/react-output-target/runtime';
import React from 'react';

type BrAvatarEvents = NonNullable<unknown>;

export const BrAvatar: StencilReactComponent<BrAvatarElement, BrAvatarEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrAvatarElement, BrAvatarEvents>({
        tagName: 'br-avatar',
        elementClass: BrAvatarElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrAvatarEvents,
        defineCustomElement: defineBrAvatar
    })
    : /*@__PURE__*/ createSSRComponent<BrAvatarElement, BrAvatarEvents>({
        tagName: 'br-avatar',
        properties: {
            density: 'density',
            customId: 'custom-id',
            src: 'src',
            alt: 'alt',
            isIconic: 'is-iconic',
            text: 'text',
            iconWidth: 'icon-width',
            iconHeight: 'icon-height',
            iconMarginTop: 'icon-margin-top',
            disabled: 'disabled'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrButtonEvents = NonNullable<unknown>;

export const BrButton: StencilReactComponent<BrButtonElement, BrButtonEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrButtonElement, BrButtonEvents>({
        tagName: 'br-button',
        elementClass: BrButtonElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrButtonEvents,
        defineCustomElement: defineBrButton
    })
    : /*@__PURE__*/ createSSRComponent<BrButtonElement, BrButtonEvents>({
        tagName: 'br-button',
        properties: {
            colorMode: 'color-mode',
            customId: 'custom-id',
            density: 'density',
            disabled: 'disabled',
            emphasis: 'emphasis',
            isActive: 'is-active',
            isLoading: 'is-loading',
            shape: 'shape',
            type: 'type',
            value: 'value'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrCheckboxEvents = {
    onCheckedChange: EventName<CustomEvent<boolean>>,
    onIndeterminateChange: EventName<CustomEvent<boolean>>
};

export const BrCheckbox: StencilReactComponent<BrCheckboxElement, BrCheckboxEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrCheckboxElement, BrCheckboxEvents>({
        tagName: 'br-checkbox',
        elementClass: BrCheckboxElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {
            onCheckedChange: 'checkedChange',
            onIndeterminateChange: 'indeterminateChange'
        } as BrCheckboxEvents,
        defineCustomElement: defineBrCheckbox
    })
    : /*@__PURE__*/ createSSRComponent<BrCheckboxElement, BrCheckboxEvents>({
        tagName: 'br-checkbox',
        properties: {
            indeterminate: 'indeterminate',
            checked: 'checked',
            customId: 'custom-id',
            disabled: 'disabled',
            hasHiddenLabel: 'has-hidden-label',
            label: 'label',
            name: 'name',
            state: 'state',
            value: 'value'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrCheckgroupEvents = NonNullable<unknown>;

export const BrCheckgroup: StencilReactComponent<BrCheckgroupElement, BrCheckgroupEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrCheckgroupElement, BrCheckgroupEvents>({
        tagName: 'br-checkgroup',
        elementClass: BrCheckgroupElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrCheckgroupEvents,
        defineCustomElement: defineBrCheckgroup
    })
    : /*@__PURE__*/ createSSRComponent<BrCheckgroupElement, BrCheckgroupEvents>({
        tagName: 'br-checkgroup',
        properties: {
            label: 'label',
            labelSelecionado: 'label-selecionado',
            labelDesselecionado: 'label-desselecionado',
            indeterminate: 'indeterminate',
            customId: 'custom-id'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrDropdownEvents = { onBrDropdownChange: EventName<CustomEvent<{ 'is-opened': boolean }>> };

export const BrDropdown: StencilReactComponent<BrDropdownElement, BrDropdownEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrDropdownElement, BrDropdownEvents>({
        tagName: 'br-dropdown',
        elementClass: BrDropdownElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onBrDropdownChange: 'brDropdownChange' } as BrDropdownEvents,
        defineCustomElement: defineBrDropdown
    })
    : /*@__PURE__*/ createSSRComponent<BrDropdownElement, BrDropdownEvents>({
        tagName: 'br-dropdown',
        properties: {
            isOpen: 'is-open',
            customId: 'custom-id'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrIconEvents = NonNullable<unknown>;

export const BrIcon: StencilReactComponent<BrIconElement, BrIconEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrIconElement, BrIconEvents>({
        tagName: 'br-icon',
        elementClass: BrIconElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrIconEvents,
        defineCustomElement: defineBrIcon
    })
    : /*@__PURE__*/ createSSRComponent<BrIconElement, BrIconEvents>({
        tagName: 'br-icon',
        properties: {
            iconName: 'icon-name',
            height: 'height',
            customId: 'custom-id',
            width: 'width',
            cssClasses: 'css-classes',
            isInline: 'is-inline',
            rotate: 'rotate',
            flip: 'flip'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrInputEvents = { onValueChange: EventName<CustomEvent<string>> };

export const BrInput: StencilReactComponent<BrInputElement, BrInputEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrInputElement, BrInputEvents>({
        tagName: 'br-input',
        elementClass: BrInputElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onValueChange: 'valueChange' } as BrInputEvents,
        defineCustomElement: defineBrInput
    })
    : /*@__PURE__*/ createSSRComponent<BrInputElement, BrInputEvents>({
        tagName: 'br-input',
        properties: {
            type: 'type',
            autocomplete: 'autocomplete',
            density: 'density',
            disabled: 'disabled',
            isInline: 'is-inline',
            isHighlight: 'is-highlight',
            state: 'state',
            label: 'label',
            customId: 'custom-id',
            name: 'name',
            placeholder: 'placeholder',
            readonly: 'readonly',
            required: 'required',
            value: 'value',
            helpText: 'help-text',
            autocorrect: 'autocorrect',
            min: 'min',
            max: 'max',
            minlength: 'minlength',
            maxlength: 'maxlength',
            multiple: 'multiple',
            pattern: 'pattern',
            step: 'step'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrItemEvents = {
    onBrDidClick: EventName<CustomEvent<any>>,
    onBrDidSelect: EventName<CustomEvent<{ selected: boolean }>>
};

export const BrItem: StencilReactComponent<BrItemElement, BrItemEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrItemElement, BrItemEvents>({
        tagName: 'br-item',
        elementClass: BrItemElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {
            onBrDidClick: 'brDidClick',
            onBrDidSelect: 'brDidSelect'
        } as BrItemEvents,
        defineCustomElement: defineBrItem
    })
    : /*@__PURE__*/ createSSRComponent<BrItemElement, BrItemEvents>({
        tagName: 'br-item',
        properties: {
            disabled: 'disabled',
            customId: 'custom-id',
            isActive: 'is-active',
            isSelected: 'is-selected',
            isInteractive: 'is-interactive',
            href: 'href',
            target: 'target',
            isButton: 'is-button',
            type: 'type',
            value: 'value',
            density: 'density'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrListEvents = NonNullable<unknown>;

export const BrList: StencilReactComponent<BrListElement, BrListEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrListElement, BrListEvents>({
        tagName: 'br-list',
        elementClass: BrListElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrListEvents,
        defineCustomElement: defineBrList
    })
    : /*@__PURE__*/ createSSRComponent<BrListElement, BrListEvents>({
        tagName: 'br-list',
        properties: {
            header: 'header',
            isHorizontal: 'is-horizontal',
            customId: 'custom-id',
            hideHeaderDivider: 'hide-header-divider'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrLoadingEvents = NonNullable<unknown>;

export const BrLoading: StencilReactComponent<BrLoadingElement, BrLoadingEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrLoadingElement, BrLoadingEvents>({
        tagName: 'br-loading',
        elementClass: BrLoadingElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrLoadingEvents,
        defineCustomElement: defineBrLoading
    })
    : /*@__PURE__*/ createSSRComponent<BrLoadingElement, BrLoadingEvents>({
        tagName: 'br-loading',
        properties: {
            isProgress: 'is-progress',
            customId: 'custom-id',
            progressPercent: 'progress-percent',
            isMedium: 'is-medium',
            label: 'label'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrMessageEvents = { onBrDidClose: EventName<CustomEvent<any>> };

export const BrMessage: StencilReactComponent<BrMessageElement, BrMessageEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrMessageElement, BrMessageEvents>({
        tagName: 'br-message',
        elementClass: BrMessageElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onBrDidClose: 'brDidClose' } as BrMessageEvents,
        defineCustomElement: defineBrMessage
    })
    : /*@__PURE__*/ createSSRComponent<BrMessageElement, BrMessageEvents>({
        tagName: 'br-message',
        properties: {
            messageTitle: 'message-title',
            customId: 'custom-id',
            message: 'message',
            isInline: 'is-inline',
            isClosable: 'is-closable',
            autoRemove: 'auto-remove',
            showIcon: 'show-icon',
            isFeedback: 'is-feedback',
            state: 'state'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrRadioEvents = { onCheckedChange: EventName<CustomEvent<boolean>> };

export const BrRadio: StencilReactComponent<BrRadioElement, BrRadioEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrRadioElement, BrRadioEvents>({
        tagName: 'br-radio',
        elementClass: BrRadioElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onCheckedChange: 'checkedChange' } as BrRadioEvents,
        defineCustomElement: defineBrRadio
    })
    : /*@__PURE__*/ createSSRComponent<BrRadioElement, BrRadioEvents>({
        tagName: 'br-radio',
        properties: {
            checked: 'checked',
            disabled: 'disabled',
            state: 'state',
            hasHiddenLabel: 'has-hidden-label',
            customId: 'custom-id',
            name: 'name',
            label: 'label',
            value: 'value'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrSelectEvents = { onBrDidSelectChange: EventName<CustomEvent<any>> };

export const BrSelect: StencilReactComponent<BrSelectElement, BrSelectEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrSelectElement, BrSelectEvents>({
        tagName: 'br-select',
        elementClass: BrSelectElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onBrDidSelectChange: 'brDidSelectChange' } as BrSelectEvents,
        defineCustomElement: defineBrSelect
    })
    : /*@__PURE__*/ createSSRComponent<BrSelectElement, BrSelectEvents>({
        tagName: 'br-select',
        properties: {
            label: 'label',
            placeholder: 'placeholder',
            isMultiple: 'is-multiple',
            options: 'options',
            selectAllLabel: 'select-all-label',
            unselectAllLabel: 'unselect-all-label',
            showSearchIcon: 'show-search-icon',
            isOpen: 'is-open',
            customId: 'custom-id'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrSwitchEvents = { onCheckedChange: EventName<CustomEvent<boolean>> };

export const BrSwitch: StencilReactComponent<BrSwitchElement, BrSwitchEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrSwitchElement, BrSwitchEvents>({
        tagName: 'br-switch',
        elementClass: BrSwitchElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onCheckedChange: 'checkedChange' } as BrSwitchEvents,
        defineCustomElement: defineBrSwitch
    })
    : /*@__PURE__*/ createSSRComponent<BrSwitchElement, BrSwitchEvents>({
        tagName: 'br-switch',
        properties: {
            disabled: 'disabled',
            checked: 'checked',
            customId: 'custom-id',
            label: 'label',
            labelPosition: 'label-position',
            labelOn: 'label-on',
            labelOff: 'label-off',
            hasIcon: 'has-icon',
            name: 'name',
            value: 'value',
            density: 'density'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrTagEvents = { onRadioSelected: EventName<CustomEvent<string>> };

export const BrTag: StencilReactComponent<BrTagElement, BrTagEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrTagElement, BrTagEvents>({
        tagName: 'br-tag',
        elementClass: BrTagElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onRadioSelected: 'radioSelected' } as BrTagEvents,
        defineCustomElement: defineBrTag
    })
    : /*@__PURE__*/ createSSRComponent<BrTagElement, BrTagEvents>({
        tagName: 'br-tag',
        properties: {
            label: 'label',
            iconName: 'icon-name',
            name: 'name',
            multiple: 'multiple',
            selected: 'selected',
            customId: 'custom-id',
            color: 'color',
            density: 'density',
            shape: 'shape',
            status: 'status',
            interaction: 'interaction',
            interactionSelect: 'interaction-select',
            disabled: 'disabled'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrTextareaEvents = { onValueChange: EventName<CustomEvent<string>> };

export const BrTextarea: StencilReactComponent<BrTextareaElement, BrTextareaEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrTextareaElement, BrTextareaEvents>({
        tagName: 'br-textarea',
        elementClass: BrTextareaElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: { onValueChange: 'valueChange' } as BrTextareaEvents,
        defineCustomElement: defineBrTextarea
    })
    : /*@__PURE__*/ createSSRComponent<BrTextareaElement, BrTextareaEvents>({
        tagName: 'br-textarea',
        properties: {
            value: 'value',
            label: 'label',
            placeholder: 'placeholder',
            customId: 'custom-id',
            disabled: 'disabled',
            maxlength: 'maxlength',
            showCounter: 'show-counter',
            state: 'state',
            density: 'density',
            isInline: 'is-inline'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });

type BrUploadEvents = NonNullable<unknown>;

export const BrUpload: StencilReactComponent<BrUploadElement, BrUploadEvents> = typeof window !== 'undefined'
    ? /*@__PURE__*/ createComponent<BrUploadElement, BrUploadEvents>({
        tagName: 'br-upload',
        elementClass: BrUploadElement,
        // @ts-ignore - React type of Stencil Output Target may differ from the React version used in the Nuxt.js project, this can be ignored.
        react: React,
        events: {} as BrUploadEvents,
        defineCustomElement: defineBrUpload
    })
    : /*@__PURE__*/ createSSRComponent<BrUploadElement, BrUploadEvents>({
        tagName: 'br-upload',
        properties: {
            accept: 'accept',
            label: 'label',
            disabled: 'disabled',
            state: 'state',
            multiple: 'multiple'
        },
        hydrateModule: import('@govbr-ds/webcomponents/dist/hydrate')
    });
