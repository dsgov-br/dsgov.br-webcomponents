module WbcPlugin
  def has_any_fluxo_label?
    labels.grep(/^fluxo::/).any?
  end

  def has_fluxo_mr_label?
    labels.grep(/^fluxo::merge-request/).any?
  end

  def has_priority_label?
    labels.grep(/^prioridade::/).any?
  end

  def has_category_label?
    labels.grep(/^categoria::/).any?
  end

  def has_tipo_label?
    labels.grep(/^tipo::/).any?
  end

  def has_pending_label?
    labels.grep(/^status::pendente$/).any?
  end

  def has_suspended_label?
    labels.grep(/^status::suspenso$/).any?
  end

  def has_impacted_label?
    labels.grep(/^status::impactado$/).any?
  end

  def has_any_label_except_fluxo_aberta?
    labels.any? { |label| label != 'fluxo::aberta' }
  end

  def has_role_labels?
    specific_labels = ['design', 'designops', 'dev', 'devops', 'gestão', 'acessibilidade']
    labels.any? { |label| specific_labels.include?(label) }
  end

  def labels
    resource[:labels]
  end

  def all_children_closed?
    issues = fetch_children_issues
    epics = fetch_children_epics

    if issues.any? && epics.any?
      return issues.all? { |issue| issue['state'] == 'closed' } && epics.all? { |epic| epic['state'] == 'closed' }
    elsif issues.any?
      return issues.all? { |issue| issue['state'] == 'closed' }
    elsif epics.any?
      return epics.all? { |epic| epic['state'] == 'closed' }
    else
      return false
    end
  end

  def fetch_children_issues
    epic_issues_url = "#{resource['_links']['self']}/issues"
    response = HTTParty.get(epic_issues_url, headers: { 'PRIVATE-TOKEN' => ENV['TRIAGE_GITLAB_API_TOKEN'] })
    JSON.parse(response.body) rescue []
  end

  def fetch_children_epics
    epic_epics_url = "#{resource['_links']['self']}/epics"
    response = HTTParty.get(epic_epics_url, headers: { 'PRIVATE-TOKEN' => ENV['TRIAGE_GITLAB_API_TOKEN'] })
    JSON.parse(response.body) rescue []
  end

  def iteration_expired?
    return false unless resource[:iteration]
    due_date = Date.parse(resource[:iteration][:due_date])
    { expired: due_date < Date.today, name: resource[:iteration][:title], due_date: due_date }
  end

  def milestone_expired?
    return false unless resource[:milestone]
    due_date = Date.parse(resource[:milestone][:due_date])
    { expired: due_date < Date.today, name: resource[:milestone][:title], due_date: due_date }
  end

  def discussion_from_group_member?
    group_member_ids = fetch_group_member_ids
    notes = fetch_notes
    notes.any? do |note|
      group_member_ids.include?(note['author']['id'])
    end
  end

  def existing_comment?(content)
    notes = fetch_notes
    notes.any? { |note| note['body'] == content }
  end

  def fetch_notes
    notes_url = "#{resource['_links']['self']}/notes"
    response = HTTParty.get(notes_url, headers: { 'PRIVATE-TOKEN' => ENV['TRIAGE_GITLAB_API_TOKEN'] })
    notes = JSON.parse(response.body) rescue []
    notes.select { |note| note['system'] == false }
  end

  def fetch_group_member_ids
    return @group_member_ids if @group_member_ids

    group_id = '13519293'
    members_url = "https://gitlab.com/api/v4/groups/#{group_id}/members"
    response = HTTParty.get(members_url, headers: { 'PRIVATE-TOKEN' => ENV['TRIAGE_GITLAB_API_TOKEN'] })
    members = JSON.parse(response.body) rescue []
    @group_member_ids = members.map { |member| member['id'] }.reject { |id| id == 9539143 }
  end
end

Gitlab::Triage::Resource::Context.include WbcPlugin
