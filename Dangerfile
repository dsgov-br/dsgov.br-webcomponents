# frozen_string_literal: true

# Documentações e exemplos auxiliares sobre o Danger
# https://danger.systems
# https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles
# https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles/-/blob/master/lib/danger/plugins/internal/helper.rb
# https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
# https://www.rubydoc.info/gems/gitlab/Gitlab/Client
# https://rubygems.org/gems/danger-gitlab

gitlab.dismiss_out_of_range_messages

require 'gitlab-dangerfiles'
require 'time'

Gitlab::Dangerfiles.for_project(self) do |dangerfiles|
  dangerfiles.import_plugins
end

# Variáveis
merge_request = gitlab.api.merge_request(helper.mr_target_project_id, helper.mr_iid).to_hash
author = merge_request['author']
group_members = gitlab.api.group_members(13519293, { per_page: 100 })
author_is_group_member = group_members.any? { |user| user["id"] == author['id'] }
# IDs são imutáveis, então usamos o ID para pegar os dados dos usuários
project_members_ids  = [10387881, 9784078, 4884162, 9782613]
project_members = group_members.select { |user| project_members_ids.include?(user["id"]) }

fail(":clipboard: A descrição do MR é muito curta. Por favor, forneça uma descrição mais detalhada com pelo menos 100 caracteres.") if helper.mr_description.size < 100
fail("Este MR não possui um milestone definido. Por favor, selecione um.") if helper.mr_milestone.nil?
fail("Nenhuma label foi adicionada ao MR. Adicione pelo menos uma.") if !helper.mr_labels
fail("É necessário adicionar uma label com o escopo `tipo`. Por exemplo `tipo::correção`.") if !helper.has_scoped_label_with_scope?("tipo")
fail("É necessário adicionar uma label com o escopo `categoria`. Por exemplo `categoria::documentação`.") if !helper.has_scoped_label_with_scope?("categoria")
fail("MR não precisam de uma label do escopo `fluxo`. Por favor, remova.") if helper.has_scoped_label_with_scope?("fluxo")

# Se não houver alterações em arquivos, isso pode indicar um problema
if git.modified_files.empty? && git.added_files.empty? && git.deleted_files.empty?
  fail 'Este PR não contém alterações. Verifique se as alterações foram incluídas corretamente.'
end

warn "#{gitlab.html_link("Package.json")} foi modificado." if git.modified_files.include? "Package.json"

package_json = git.modified_files.include? "package.json"
lockfile = git.modified_files.include? "package-lock.json"
warn("O PR modificou o package.json, mas não o package-lock.json. Certifique-se de atualizar ambos os arquivos.") if package_json && !lockfile

warn("Este MR faz alterações no processo de CI.") if helper.has_ci_changes?
warn('Este pull request está marcado como RASCUNHO e ainda não está pronto para mesclar.') if helper.draft_mr?
warn('O MR contém muitas linhas de código alteradas. Verifique cuidadosamente as mudanças.') if git.lines_of_code > 500

warn "Este MR fecha alguma issue? Se sim, liste todas as issues."
warn "Este MR está configurado para deletar a branch ao ser aceito." if gitlab.mr_json["force_remove_source_branch"]
warn "Este MR não está configurado para deletar a branch ao ser aceito." if !gitlab.mr_json["force_remove_source_branch"]

def file_modified?(file_url)
  git.modified_files.include?(file_url) || git.added_files.include?(file_url) || git.deleted_files.include?(file_url)
end

danger_modified = file_modified?("Dangerfile")
triage_modified = file_modified?(".triage-policies.yml")
components_modified = file_modified?("packages/webcomponents/src/components/**/*.tsx")
components_tests_modified = file_modified?("packages/webcomponents/src/components/**/tests/*")
components_migrate_modified = file_modified?("packages/webcomponents/src/components/**/sections/migrate.md")
pages_modified = file_modified?("packages/webcomponents/src/pages/*")
site_modified = file_modified?("apps/site/*")

warn("O arquivo .triage-custom.rb foi modificado.") if git.modified_files.include? ".triage-custom.rb"
warn("O arquivo .prettierrc.yml foi modificado.") if git.modified_files.include? ".prettierrc.yml"
warn("O arquivo .prettierignore foi modificado.") if git.modified_files.include? ".prettierignore"
warn("O arquivo .markdownlintignore foi modificado.") if git.modified_files.include? ".markdownlintignore"
warn("O arquivo .markdownlint.yml foi modificado.") if git.modified_files.include? ".markdownlint.yml"
warn("O arquivo .gitlab-ci.yml foi modificado.") if git.modified_files.include? ".gitlab-ci.yml"
warn("O arquivo .gitignore foi modificado.") if git.modified_files.include? ".gitignore"
warn("O arquivo .gitattributes foi modificado.") if git.modified_files.include? ".gitattributes"
warn("O arquivo .editorconfig foi modificado.") if git.modified_files.include? ".editorconfig"
warn("O arquivo .commitlintrc.js foi modificado.") if git.modified_files.include? ".commitlintrc.js"

markdown File.read(".review-checklists/default.md")

markdown File.read(".review-checklists/bots.md") if danger_modified || triage_modified

markdown File.read(".review-checklists/ci.md") if helper.has_ci_changes?

markdown File.read(".review-checklists/components.md") if components_modified

markdown File.read(".review-checklists/migrate.md") if components_migrate_modified

markdown File.read(".review-checklists/pages.md") if pages_modified

markdown File.read(".review-checklists/site.md") if site_modified

warn("Há alterações em bibliotecas, mas não foram adicionados testes. Isso é aceitável apenas se você estiver refatorando código existente.") if components_modified && !components_tests_modified

warn "O `squash commits` está ativado para este commit. Verifique se é realmente necessário." if helper.squash_mr?
warn "O `squash commits` não está ativado para este commit. Verifique se essa é realmente a sua intenção." if !helper.squash_mr?

message "🎉 Este MR foi criado por um membro da comunidade." if !author_is_group_member

if git.deletions > git.insertions
  message '🗑️ Limpeza de código!'
end

message 'Todos os MRs devem ser revisados pelos membros da equipe.'
message 'Considere solicitar a revisão de um **designer** e/ou **desenvolvedor** para esclarecer eventuais dúvidas.'

# Define um responsável se nenhum estiver definido ainda
if helper.mr_assignees.empty?
  if author_is_group_member
    gitlab.api.update_merge_request(helper.mr_target_project_id, helper.mr_iid, { assignee_id: author['id'] })
    message"Responsável definido: #{author['name']} (@#{author['username']})"
  end

  # Para MR criado por um usuário da comunidade, define um usuário do projeto como responsável
  if !author_is_group_member
    assignee = project_members.sample
    gitlab.api.update_merge_request(helper.mr_target_project_id, helper.mr_iid, { assignee_id: assignee['id'] })
    message "Responsável definido: #{assignee['name']} (@#{assignee['username']})"
  end
end

# Define um revisor se nenhum estiver definido ainda
if helper.mr_assignees.empty? && !helper.draft_mr?
  reviewers = project_members.reject { |member| member['id'] == author['id'] }
  reviewer_ids = project_members.map { |member| member['id'] }

  message "Revisor definido: #{reviewer['name']} (@#{reviewer['username']})"

  gitlab.api.update_merge_request(helper.mr_target_project_id, helper.mr_iid, { reviewer_ids: reviewer_ids })
end

todoist.message = "Por favor não deixe nenhum 'TODO' no código."
todoist.warn_for_todos
todoist.print_todos_table
