#!/bin/bash

# =============================================================================
# Script para criação automática de issues padronizadas no GitLab
#
# Uso:
#   ./create_issues.sh [opções]
#
# Opções:
#   -t, --token         Token de acesso do GitLab
#   -p, --project-id    ID do projeto
#   -g, --group-id      ID do grupo
#   -e, --epic-id       ID do épico pai (opcional)
#   -l, --link-type     Tipo de link entre issues (padrão: relates_to)
#   -s, --spec-id       ID da issue de especificação existente
#   -a, --access-id     ID da issue de acessibilidade existente
#   -d, --design-id     ID da issue de design existente
#   -v, --dev-id        ID da issue de desenvolvimento existente
#   -h, --help          Mostra esta ajuda
#
# Exemplos:
#   Criar todas as issues:
#   $ ./create_issues.sh -t "seu-token" -p "123" -g "456"
#
#   Usar issues existentes:
#   $ ./create_issues.sh -t "token" -p "123" -g "456" -s "789" -a "790" -d "791" -v "792"
# =============================================================================

# Função para mostrar ajuda
show_help() {
  grep '^#' "$0" | grep -v '#!/bin/bash' | sed 's/^# \{0,1\}//'
  exit 0
}

# Valores padrão
GITLAB_API_URL="https://gitlab.com/api/v4"
LINK_TYPE="relates_to"
DRY_RUN=true # Sempre começa em dry-run

# Processar parâmetros nomeados
while [[ $# -gt 0 ]]; do
  case $1 in
  -t | --token)
    GITLAB_TOKEN="$2"
    shift 2
    ;;
  -p | --project-id)
    PROJECT_ID="$2"
    shift 2
    ;;
  -g | --group-id)
    GROUP_ID="$2"
    shift 2
    ;;
  -e | --epic-id)
    PARENT_EPIC_IID="$2"
    shift 2
    ;;
  -l | --link-type)
    LINK_TYPE="$2"
    shift 2
    ;;
  -s | --spec-id)
    SPEC_ISSUE_ID="$2"
    shift 2
    ;;
  -a | --access-id)
    ACCESS_ISSUE_ID="$2"
    shift 2
    ;;
  -d | --design-id)
    DESIGN_ISSUE_ID="$2"
    shift 2
    ;;
  -v | --dev-id)
    DEV_ISSUE_ID="$2"
    shift 2
    ;;
  -h | --help)
    show_help
    ;;
  *)
    echo "Opção inválida: $1"
    show_help
    ;;
  esac
done

# Validar parâmetros obrigatórios
if [ -z "$GITLAB_TOKEN" ] || [ -z "$PROJECT_ID" ] || [ -z "$GROUP_ID" ]; then
  echo "Erro: Token, Project ID e Group ID são obrigatórios"
  show_help
fi

# Lista de componentes para criar issues
# Adicione novos componentes aqui, separados por espaço
# Exemplo: ISSUES_LIST=("collapse" "button" "header")
ISSUES_LIST=("collapse")

# Configuração do épico
# Formato: ("Prefixo" "Descrição" "Labels")
EPIC_CONFIG=(
  "Épico do componente"                                           # Prefixo do título
  "Este épico agrupa todas as issues relacionadas ao componente." # Descrição
  "épico, categoria::componente, prioridade::P1"                  # Labels
)

# Templates de Issues
# Formato: "ID;TITULO;TEMPLATE;LABELS"
#
# ID        - Identificador único da issue (ex: SPEC, CREATE, etc)
# TITULO    - Título base da issue
# TEMPLATE  - Nome do arquivo de template em issue_templates/
# LABELS    - Labels separadas por vírgula
declare -A ISSUE_TEMPLATES=(
  ["SPEC"]="Especificação do componente;Especificação Componente;dev,categoria::componente,tipo::análise"
  ["CREATE"]="Criar o componente;Criar componente;dev,categoria::componente,tipo::novo"
  ["ACCESS"]="Revisão de acessibilidade;Revisão Acessibilidade;acessibilidade,categoria::componente,tipo::revisão"
  ["DESIGN"]="Revisão de design;Revisão Design;design,categoria::componente,tipo::revisão"
  ["DEV"]="Revisão de desenvolvimento;Revisão Dev;dev,categoria::componente,tipo::revisão"
)

# Links entre Issues e Épicos
# Formato: "ORIGEM:DESTINO:TIPO_LINK"
#
# ORIGEM/DESTINO podem ser:
#   - ID de issue do ISSUE_TEMPLATES (ex: SPEC, CREATE)
#   - Épico existente (ex: epic:123)
#   - Novo épico (ex: epic:new:Card)
#
# TIPO_LINK:
#   - blocks: Origem bloqueia destino
#   - relates_to: Issues relacionadas
#   - is_blocked_by: Origem é bloqueada por destino
#   - child_of: Origem é filha do épico destino
#   - parent_of: Origem é pai do épico destino
ISSUE_LINKS=(
  "SPEC:CREATE:blocks"       # Especificação bloqueia Criar
  "CREATE:ACCESS:blocks"     # Criar bloqueia Revisão Acessibilidade
  "CREATE:DESIGN:blocks"     # Criar bloqueia Revisão Design
  "CREATE:DEV:blocks"        # Criar bloqueia Revisão Dev
  "ACCESS:DESIGN:relates_to" # Acessibilidade relacionada com Design
  "ACCESS:epic:123:child_of" # Acessibilidade é filha do épico 123
  "SPEC:epic:456:child_of"   # Especificação é filha do épico 456
)

PROJECT_DIR=$(dirname "$(dirname "$(realpath "$0")")")

# Função para criar uma issue e retornar seu ID e IID
# Parâmetros:
#   $1 - Título da issue
#   $2 - Descrição da issue
#   $3 - Labels da issue
# Retorno:
#   ID e IID da issue criada no formato "ID:IID"
create_issue() {
  local TITLE=$1
  local DESCRIPTION=$2
  local LABELS=$3

  RESPONSE=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --data "title=$TITLE" \
    --data "description=$DESCRIPTION" \
    --data "labels=$LABELS" \
    "$GITLAB_API_URL/projects/$PROJECT_ID/issues")

  local CREATED_ID=$(echo "$RESPONSE" | jq -r '.id')
  local CREATED_IID=$(echo "$RESPONSE" | jq -r '.iid')
  echo "${CREATED_ID}:${CREATED_IID}"
}

# Função para criar um épico e retornar seu IID
# Parâmetros:
#   $1 - Título do épico
#   $2 - Descrição do épico
#   $3 - Labels do épico
# Retorno:
#   IID do épico criado
create_epic() {
  local TITLE=$1
  local DESCRIPTION=$2
  local LABELS=$3

  RESPONSE=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --data "title=$TITLE" \
    --data "description=$DESCRIPTION" \
    --data "labels=$LABELS" \
    "$GITLAB_API_URL/groups/$GROUP_ID/epics")

  echo $RESPONSE | jq -r '.iid'
}

# Função para criar relacionamento entre issues
create_issue_relationship() {
  local SOURCE_IID=$1
  local TARGET_IID=$2
  local LINK_TYPE=$3

  if [ "$DRY_RUN" = true ]; then
    echo "[SIMULAÇÃO] Criando relacionamento $LINK_TYPE entre #$SOURCE_IID e #$TARGET_IID"
    return
  fi

  curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    "$GITLAB_API_URL/projects/$PROJECT_ID/issues/$SOURCE_IID/links?target_project_id=$PROJECT_ID&target_issue_iid=$TARGET_IID&link_type=$LINK_TYPE" >/dev/null

  echo "Relacionamento $LINK_TYPE criado entre #$SOURCE_IID e #$TARGET_IID"
}

# Função para criar relacionamento entre issues/épicos
create_relationship() {
  local SOURCE=$1
  local TARGET=$2
  local LINK_TYPE=$3

  if [ "$DRY_RUN" = true ]; then
    echo "[SIMULAÇÃO] Criando relacionamento $LINK_TYPE entre $SOURCE e $TARGET"
    return
  fi

  # Determinar tipo de relacionamento (issue-issue, issue-epic, epic-epic)
  if [[ $SOURCE == epic:* ]]; then
    local SOURCE_ID=${SOURCE#epic:}
    if [[ $TARGET == epic:* ]]; then
      # Epic-Epic relationship
      local TARGET_ID=${TARGET#epic:}
      curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
        "$GITLAB_API_URL/groups/$GROUP_ID/epics/$SOURCE_ID/links/$TARGET_ID" >/dev/null
    else
      # Epic-Issue relationship
      curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
        "$GITLAB_API_URL/groups/$GROUP_ID/epics/$SOURCE_ID/issues/$TARGET" >/dev/null
    fi
  else
    # Issue-Issue relationship
    curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
      "$GITLAB_API_URL/projects/$PROJECT_ID/issues/$SOURCE/links?target_project_id=$PROJECT_ID&target_issue_iid=$TARGET&link_type=$LINK_TYPE" >/dev/null
  fi

  echo "Relacionamento $LINK_TYPE criado entre $SOURCE e $TARGET"
}

# Função: process_epic_config
# -------------------------
# Processa a configuração de um épico e cria suas issues
#
# Parâmetros:
#   $1 - EPIC_KEY: Identificador do épico ou configuração de novo épico
#   $2 - CONFIG: String de configuração no formato "TIPOS:SUFIXO[:TEMPLATE]"
#
# Comportamento:
#   1. Cria/usa épico conforme EPIC_KEY
#   2. Cria issues conforme TIPOS
#   3. Estabelece relacionamentos entre issues
#   4. Configura bloqueios entre issues
process_epic_config() {
  local EPIC_KEY=$1
  local CONFIG=$2
  declare -A CREATED_ISSUES # Array para armazenar IDs das issues criadas

  # Extrair tipos de issues, sufixo e template
  IFS=":" read -r ISSUE_TYPES_STR SUFFIX TEMPLATE_NAME <<<"$CONFIG"

  if [ "$DRY_RUN" = true ]; then
    echo -e "\n[SIMULAÇÃO] Processando épico: $EPIC_KEY"
    echo "--------------------------------------------------------------------------------"
  fi

  # Se começa com "new:", criar novo épico
  if [[ $EPIC_KEY == new:* ]]; then
    IFS=":" read -r _ EPIC_TITLE EPIC_DESC EPIC_LABELS <<<"$EPIC_KEY"
    local DESCRIPTION="$EPIC_DESC"
    # Se um template foi especificado, carregá-lo
    if [ ! -z "$TEMPLATE_NAME" ]; then
      DESCRIPTION=$(load_epic_template "$TEMPLATE_NAME")
    fi
    local EPIC_IID=$(create_epic "$EPIC_TITLE" "$DESCRIPTION" "$EPIC_LABELS")
    echo "Novo épico '$EPIC_TITLE' criado com IID $EPIC_IID usando template: $TEMPLATE_NAME"
  else
    local EPIC_IID=$EPIC_KEY
  fi

  # Criar array com tipos de issues
  IFS="," read -ra SELECTED_TYPES <<<"$ISSUE_TYPES_STR"

  # Para cada tipo de issue selecionado
  for TYPE_ID in "${SELECTED_TYPES[@]}"; do
    if [[ -n "${ISSUE_TEMPLATES[$TYPE_ID]}" ]]; then
      IFS=";" read -r TITLE TEMPLATE LABELS <<<"${ISSUE_TEMPLATES[$TYPE_ID]}"
      local FULL_TITLE="$TITLE $SUFFIX"
      local DESCRIPTION=$(load_template "$TEMPLATE")

      # Criar issue
      local ISSUE_DATA=$(create_issue "$FULL_TITLE" "$DESCRIPTION" "$LABELS")
      local ISSUE_ID=$(echo "$ISSUE_DATA" | cut -d: -f1)
      local ISSUE_IID=$(echo "$ISSUE_DATA" | cut -d: -f2)

      # Armazenar ID da issue criada
      CREATED_ISSUES[$TYPE_ID]=$ISSUE_IID

      # Relacionar ao épico
      if [ "$DRY_RUN" = false ]; then
        curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
          "$GITLAB_API_URL/groups/$GROUP_ID/epics/$EPIC_IID/issues/$ISSUE_ID" >/dev/null
      fi

      echo "Issue '$FULL_TITLE' (#$ISSUE_IID) criada e relacionada ao épico $EPIC_IID"
    fi
  done

  # Processar relacionamentos
  for LINK in "${ISSUE_LINKS[@]}"; do
    IFS=":" read -r SOURCE TARGET TYPE <<<"$LINK"

    if [[ -n "${CREATED_ISSUES[$SOURCE]}" && -n "${CREATED_ISSUES[$TARGET]}" ]]; then
      create_relationship "${CREATED_ISSUES[$SOURCE]}" "${CREATED_ISSUES[$TARGET]}" "$TYPE"
    elif [[ -n "${CREATED_ISSUES[$SOURCE]}" && "$TARGET" == epic:* ]]; then
      create_relationship "${CREATED_ISSUES[$SOURCE]}" "$TARGET" "$TYPE"
    elif [[ "$SOURCE" == epic:* && -n "${CREATED_ISSUES[$TARGET]}" ]]; then
      create_relationship "$SOURCE" "${CREATED_ISSUES[$TARGET]}" "$TYPE"
    fi
  done
}

# Se PARENT_EPIC_IID não for fornecido, criar o épico primeiro
if [ -z "$PARENT_EPIC_IID" ]; then
  TITLE="${EPIC_CONFIG[0]} ${ISSUES_LIST[0]}"
  DESCRIPTION="${EPIC_CONFIG[1]}"
  PARENT_EPIC_IID=$(create_epic "$TITLE" "$DESCRIPTION" "${EPIC_CONFIG[2]}")
  if [ -z "$PARENT_EPIC_IID" ]; then
    echo "Falha ao criar o épico"
    exit 1
  fi
  echo "Épico '$TITLE' criado com IID $PARENT_EPIC_IID"
fi

# Iterar sobre a lista de componentes
for ISSUE in "${ISSUES_LIST[@]}"; do
  # Criar ou usar issue de especificação existente
  if [ -z "$SPEC_ISSUE_ID" ]; then
    IFS=";" read -r SPEC_PREFIX SPEC_TEMPLATE SPEC_LABELS <<<"${ISSUE_TYPES[0]}"
    SPEC_DESCRIPTION=$(load_template "$SPEC_TEMPLATE")
    SPEC_TITLE="$SPEC_PREFIX $ISSUE"
    ISSUE_DATA=$(create_issue "$SPEC_TITLE" "$SPEC_DESCRIPTION" "$SPEC_LABELS")
    SPEC_ISSUE_ID=$(echo "$ISSUE_DATA" | cut -d: -f1)
    SPEC_ISSUE_IID=$(echo "$ISSUE_DATA" | cut -d: -f2)
    echo "Issue '$SPEC_TITLE' criada com ID $SPEC_ISSUE_ID"
  else
    SPEC_ISSUE_IID=$SPEC_ISSUE_ID
    echo "Usando issue de especificação existente com ID $SPEC_ISSUE_ID"
  fi

  # Relacionar ao épico se existir
  if [ ! -z "$PARENT_EPIC_IID" ]; then
    curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
      "$GITLAB_API_URL/groups/$GROUP_ID/epics/$PARENT_EPIC_IID/issues/$SPEC_ISSUE_ID" >/dev/null
    echo "Issue de especificação relacionada ao épico $PARENT_EPIC_IID"
  fi

  # Array de IDs de issues e seus índices correspondentes
  declare -A ISSUE_IDS=([1]="$ACCESS_ISSUE_ID" [2]="$DESIGN_ISSUE_ID" [3]="$DEV_ISSUE_ID")

  # Criar ou usar as demais issues
  for ((i = 1; i < ${#ISSUE_TYPES[@]}; i++)); do
    if [ -z "${ISSUE_IDS[$i]}" ]; then
      # Criar nova issue
      IFS=";" read -r PREFIX TEMPLATE LABELS <<<"${ISSUE_TYPES[$i]}"
      DESCRIPTION=$(load_template "$TEMPLATE")
      TITLE="$PREFIX $ISSUE"
      ISSUE_DATA=$(create_issue "$TITLE" "$DESCRIPTION" "$LABELS")
      NEW_ISSUE_ID=$(echo "$ISSUE_DATA" | cut -d: -f1)
      NEW_ISSUE_IID=$(echo "$ISSUE_DATA" | cut -d: -f2)
      echo "Issue '$TITLE' criada com ID $NEW_ISSUE_ID"
    else
      # Usar issue existente
      NEW_ISSUE_ID=${ISSUE_IDS[$i]}
      NEW_ISSUE_IID=${ISSUE_IDS[$i]}
      echo "Usando issue existente com ID ${ISSUE_IDS[$i]}"
    fi

    # Relacionar ao épico se existir
    if [ ! -z "$PARENT_EPIC_IID" ]; then
      curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
        "$GITLAB_API_URL/groups/$GROUP_ID/epics/$PARENT_EPIC_IID/issues/$NEW_ISSUE_ID" >/dev/null
      echo "Issue relacionada ao épico $PARENT_EPIC_IID"
    fi

    # Relacionar com a issue de especificação
    curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
      "$GITLAB_API_URL/projects/$PROJECT_ID/issues/$NEW_ISSUE_IID/links?target_project_id=$PROJECT_ID&target_issue_iid=$SPEC_ISSUE_IID&link_type=is_blocked_by" >/dev/null
    echo "Issue relacionada à especificação"
  done
done
