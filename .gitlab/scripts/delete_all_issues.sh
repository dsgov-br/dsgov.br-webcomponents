#!/bin/bash

# Script para deletar issues do GitLab
# Uso: ./delete_all_issues.sh [issue_iid1 issue_iid2 ...]
# Se nenhum número de issue for fornecido, todas as issues serão deletadas

# Valores padrão
GITLAB_API_URL="https://gitlab.com/api/v4"
DRY_RUN=true # Sempre começa em dry-run
ISSUE_STATE="opened"
declare -a ISSUE_IIDS=()

if [ -z "$GITLAB_TOKEN" ] || [ -z "$PROJECT_ID" ]; then
  echo "Erro: GITLAB_TOKEN e PROJECT_ID precisam ser configurados no script"
  exit 1
fi

delete_issue() {
  local issue_iid=$1
  delete_response=$(curl --silent --request DELETE --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/issues/$issue_iid")
  echo "Issue $issue_iid deletada: $delete_response"
}

# Se argumentos foram fornecidos, delete apenas as issues específicas
if [ $# -gt 0 ]; then
  echo "Deletando issues específicas: $@"
  for issue_iid in "$@"; do
    delete_issue "$issue_iid"
  done
else
  # Caso contrário, delete todas as issues
  echo "Obtendo todas as issues do projeto..."
  response=$(curl --silent --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/issues?per_page=100")

  if [ -z "$response" ] || [ "$response" = "null" ]; then
    echo "Nenhuma issue encontrada ou erro na requisição"
    exit 1
  fi

  echo "Deletando todas as issues..."
  echo "$response" | jq -r '.[].iid' | while read issue_iid; do
    delete_issue "$issue_iid"
  done
fi
