#!/bin/bash

# =============================================================================
# Script para criação automática de issues padronizadas no GitLab
#
# Uso:
#   ./create_issues.sh [opções]
#
# Opções:
#   -t, --token         Token de acesso do GitLab
#   -p, --project-id    ID do projeto
#   -h, --help          Mostra esta ajuda
#
# Exemplos:
#   Criar as issues com token e projeto:
#   $ ./create_issues.sh -t "seu-token" -p "123"
# =============================================================================

# Função para mostrar ajuda
show_help() {
  grep '^#' "$0" | grep -v '#!/bin/bash' | sed 's/^# \{0,1\}//'
  exit 0
}

GITLAB_API_URL="https://gitlab.com/api/v4"
DRY_RUN=true

while [[ $# -gt 0 ]]; do
  case $1 in
  -t | --token)
    GITLAB_TOKEN="$2"
    shift 2
    ;;
  -p | --project-id)
    PROJECT_ID="$2"
    shift 2
    ;;
  -h | --help)
    show_help
    ;;
  *)
    echo "Opção inválida: $1"
    show_help
    ;;
  esac
done

# Função para criar uma issue
create_issue() {
  local title=$1
  local template_file="../issue_templates/Revisão Design.md"

  if [[ ! -f "$template_file" ]]; then
    echo "Erro: Arquivo de template não encontrado: $template_file"
    exit 1
  fi

  # Escapa caracteres especiais da descrição
  local description=$(cat "$template_file" | sed 's/"/\\"/g' | sed ':a;N;$!ba;s/\n/\\n/g')

  local json_data="{\"title\":\"$title\",\"description\":\"$description\",\"labels\":[\"categoria::componente\",\"design\",\"tipo::revisão\"]}"

  curl --request POST \
    --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --header "Content-Type: application/json" \
    --data "$json_data" \
    "$GITLAB_API_URL/projects/$PROJECT_ID/issues"
}

# Lista de componentes dentro do script
COMPONENTS=("Avatar" "Button")

baseTitle="Revisão Design"

# Função para criar issues para cada sufixo
create_issues_with_suffixes() {
  local suffixes=$1
  for suffix in $suffixes; do
    local title="$suffix - $baseTitle"
    create_issue "$title" "$DESCRIPTION" "$labels"
  done
}

# Utilizar a lista de componentes para criar issues
create_issues_with_suffixes "${COMPONENTS[*]}"
