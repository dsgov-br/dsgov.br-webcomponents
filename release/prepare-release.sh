#!/bin/bash

set -e

pnpm dlx nx release version $1
pnpm run build --verbose

# Verificar se o parâmetro passado é "next", "alpha", "beta" ou "rc"
if [[ ! "$1" =~ (next|alpha|beta|rc) ]]; then
  pnpm dlx nx docs:version site $1 --w=site
else
  echo "Versão $1 é uma versão de pré-lançamento (next, alpha, beta, rc). Não será gerada uma versão da documentação no site."
fi
